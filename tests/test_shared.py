import pytest
from bs4 import BeautifulSoup
from game_parser import get_game_info
from critics_parser import get_all_critic_reviews
from shared import get_html, unexcepted_none, unexcepted_float
from users_parser import get_users_next_page


GAMES_URL = (
    'https://www.metacritic.com/game/playstation-4/death-stranding',
    'https://www.metacritic.com/game/playstation-4/control',
    'https://www.metacritic.com/game/playstation-4/star-wars-jedi-fallen-order',
)


def test_get_game_info():
    for url in GAMES_URL:
        game_info = get_game_info(url)
        assert game_info


def test_get_all_critic_reviews():
    for url in GAMES_URL:
        critics = get_all_critic_reviews(url)
        assert critics


def test_get_all_critic_reviews():
    for url in GAMES_URL:
        critics = get_all_critic_reviews(url)
        assert critics

def test_get_html():
    for url in GAMES_URL:
        html = get_html(url)
        assert html


def test_unexcepted_none():
    @unexcepted_none
    def _return_true():
        return True

    assert _return_true()


def test_unexcepted_none_2():
    @unexcepted_none
    def _return_exception():
        raise Exception()
        return True

    assert _return_exception() is None


def test_unexcepted_float_to_float():
    @unexcepted_float
    def _return_float():
        return '3.2'

    assert _return_float() == 3.2


def test_unexcepted_float_to_str():
    @unexcepted_float
    def _return_not_float():
        return 'Not float'

    assert _return_not_float() is None


def test_next_user_from_1():
    soup = BeautifulSoup(
        get_html('https://www.metacritic.com/game/playstation-4/star-wars-jedi-fallen-order/user-reviews'),
        'lxml'
    )
    next_url = get_users_next_page(soup)
    assert next_url == 'https://www.metacritic.com/game/playstation-4/star-wars-jedi-fallen-order/user-reviews?page=1'


def test_next_user_from_5():
    soup = BeautifulSoup(
        get_html('https://www.metacritic.com/game/playstation-4/star-wars-jedi-fallen-order/user-reviews?page=3'),
        'lxml'
    )
    next_url = get_users_next_page(soup)
    assert next_url == 'https://www.metacritic.com/game/playstation-4/star-wars-jedi-fallen-order/user-reviews?page=4'


def test_next_user_from_last():
    soup = BeautifulSoup(
        get_html('https://www.metacritic.com/game/playstation-4/star-wars-jedi-fallen-order/user-reviews?page=6'),
        'lxml'
    )
    next_url = get_users_next_page(soup)
    assert next_url is None


def test_next_games_from_1():
    soup = BeautifulSoup(
        get_html('https://www.metacritic.com/browse/games/score/metascore/all/ps4/filtered'),
        'lxml'
    )
    next_url = get_users_next_page(soup)
    assert next_url == 'https://www.metacritic.com/browse/games/score/metascore/all/ps4/filtered?page=1'


def test_next_games_from_5():
    soup = BeautifulSoup(
        get_html('https://www.metacritic.com/browse/games/score/metascore/all/ps4/filtered?page=5'),
        'lxml'
    )
    next_url = get_users_next_page(soup)
    assert next_url == 'https://www.metacritic.com/browse/games/score/metascore/all/ps4/filtered?page=6'
