import traceback
import logging
import inspect
import requests
from user_agent import generate_user_agent

logging.basicConfig(filename='metacritic.log', level=logging.DEBUG)


def unexcepted_float(func):
    def wrapper(*args, **kwargs):
        try:
            result = float(func(*args, **kwargs))
        except Exception as ex:
            logger = logging.getLogger(inspect.getmodule(func).__name__)
            logger.info(ex)
            traceback.print_exc()
            result = None

        return result
    return wrapper


def unexcepted_none(func):
    def wrapper(*args, **kwargs):
        try:
            result = func(*args, **kwargs)
        except Exception as ex:
            logger = logging.getLogger(inspect.getmodule(func).__name__)
            logger.info(ex)
            traceback.print_exc()
            result = None

        return result
    return wrapper


def unexcepted_result(func):
    def wrapper(*args, **kwargs):
        while True:
            try:
                result = func(*args, **kwargs)
                return result
            except Exception as ex:
                logger = logging.getLogger(inspect.getmodule(func).__name__)
                logger.info(ex)
                traceback.print_exc()
    return wrapper


@unexcepted_result
def get_html(url):
    headers = {'User-Agent': generate_user_agent(device_type="desktop", os=('mac', 'linux'))}
    page_response = requests.get(url, timeout=30, headers=headers)
    return page_response.text


def log_function(func):
    def wrapper(*args, **kwargs):
        logger = logging.getLogger(inspect.getmodule(func).__name__)
        logger.info('Start func: %s; args: %s; kwargs: %s', func.__name__, args, kwargs)
        return func(*args, **kwargs)

    return wrapper
