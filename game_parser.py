from collections import namedtuple
from bs4 import BeautifulSoup
from shared import get_html, log_function, unexcepted_result, unexcepted_none, unexcepted_float
GameInfo = namedtuple('GameInfo', 'url title metascore userscore developers genres rating release_date publisher')


@unexcepted_float
def get_metascore(soup):
    metascore = soup.find('div', class_='metascore_wrap highlight_metascore')
    metascore = metascore.find('div', class_='metascore_w').find('span')
    return metascore.contents[0].strip() if metascore else None


@unexcepted_float
def get_userscore(soup):
    userscore = soup.find('div', class_='userscore_wrap feature_userscore').find('div', class_='metascore_w')
    return userscore.contents[0].strip() if userscore else None


@unexcepted_none
def get_developers(soup):
    developers = soup.find('li', class_='summary_detail developer').find_all('span', class_='data')
    return [developer.contents[0].strip() for developer in developers]


@unexcepted_none
def get_genres(soup):
    genres = soup.find('li', class_='summary_detail product_genre').find_all('span', class_='data')
    return [genre.contents[0].strip() for genre in genres]


@unexcepted_none
def get_rating(soup):
    rating = soup.find('li', class_='summary_detail product_rating').find('span', class_='data')
    return rating.contents[0].strip() if rating else None


@unexcepted_none
def get_release_date(soup):
    release = soup.find('li', class_='summary_detail release_data').find('span', class_='data')
    return release.contents[0].strip() if release else None


@unexcepted_none
def get_publisher(soup):
    publisher = soup.find('li', class_='summary_detail publisher').find('a')
    return publisher.contents[0].strip() if publisher else None


def get_title(soup):
    title = soup.find('div', class_='product_title').find('h1')
    return title.contents[0].strip()


def create_game_info(soup, url):
    return GameInfo(
        url=url,
        title=get_title(soup),
        metascore=get_metascore(soup),
        userscore=get_userscore(soup),
        developers=get_developers(soup),
        genres=get_genres(soup),
        rating=get_rating(soup),
        release_date=get_release_date(soup),
        publisher=get_publisher(soup)
    )


@unexcepted_result
@log_function
def get_game_info(game_url):
    soup = BeautifulSoup(get_html(game_url), 'lxml')
    return create_game_info(soup, game_url)
