from bs4 import BeautifulSoup
from review_parser import parse_critic_review
from shared import get_html, log_function, unexcepted_result
CRITICS_POSTFIX = '/critic-reviews'


def get_critics_url(game_url):
    return game_url + CRITICS_POSTFIX


def get_critic_reviews(soup, review_parser=parse_critic_review):
    try:
        critic_reviews = soup.find('ol', class_='reviews critic_reviews').find_all('div', class_='review_content')
    except Exception:
        critic_reviews = []

    result = []
    for review in critic_reviews:
        result.append(review_parser(review))

    return result


@unexcepted_result
@log_function
def get_all_critic_reviews(game_url):
    current_url = get_critics_url(game_url)
    soup = BeautifulSoup(get_html(current_url), 'lxml')
    return get_critic_reviews(soup)
