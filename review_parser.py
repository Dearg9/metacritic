from dataclasses import dataclass
from shared import unexcepted_result


@dataclass
class BaseReview:
    """This class describe review of users or critics"""
    name: str
    url: str
    date: str
    score: str
    comment: str = None


def parse_review(soup) -> BaseReview:
    about_critic = soup.find('div', class_='review_critic')
    # get name and url
    name_critic = about_critic.find('div', class_='source')
    url_critic = about_critic.find('a')
    name = url_critic.contents[0].strip() if url_critic else name_critic.contents[0].strip()
    url = url_critic.get('href') if url_critic else ''

    date = about_critic.find('div', class_='date').contents[0].strip()
    score = soup.find('div', class_='review_grade').find('div', class_='metascore_w').contents[0].strip()
    return BaseReview(name, url, date, score)


def parse_critic_review(soup) -> BaseReview:
    critic_review = parse_review(soup)
    critic_review.comment = soup.find('div', class_='review_body').contents[0].strip()
    return critic_review


def parse_user_review(soup) -> BaseReview:
    user_review = parse_review(soup)
    review_body = soup.find('div', class_='review_body')
    expanded_span = review_body.find('span', class_='blurb blurb_expanded')
    text_span = expanded_span if expanded_span else review_body.find('span')
    user_review.comment = text_span.contents[0].strip() if text_span is not None else ''
    return user_review
