from bs4 import BeautifulSoup
from review_parser import parse_user_review
from metacritic import METACRITIC_HOSTNAME
from shared import get_html, log_function, unexcepted_result

USERS_POSTFIX = '/user-reviews'


def get_users_url(game_url):
    return game_url + USERS_POSTFIX


def get_users_next_page(soup):
    a = soup.find('a', class_='action', rel='next')
    return METACRITIC_HOSTNAME + a.get('href') if a else None


def get_user_reviews(soup, review_parser=parse_user_review):
    try:
        critic_reviews = soup.find('ol', class_='reviews user_reviews').find_all('div', class_='review_content')
    except Exception:
        critic_reviews = []

    result = []
    for review in critic_reviews:
        result.append(review_parser(review))

    return result


@unexcepted_result
@log_function
def get_all_user_reviews(game_url):
    current_url = get_users_url(game_url)
    result = []
    while current_url is not None:
        user_reviews, current_url = iter_all_user_reviews(current_url)
        result.extend(user_reviews)

    return result


@unexcepted_result
def iter_all_user_reviews(url):
    soup = BeautifulSoup(get_html(url), 'lxml')
    user_reviews = get_user_reviews(soup)
    next_url = get_users_next_page(soup)
    return user_reviews, next_url
