from bs4 import BeautifulSoup
from shared import get_html, unexcepted_result
METACRITIC_HOSTNAME = 'https://www.metacritic.com'
PAGE_POST_FORMATTER = '?page={page}'


class PlatformName:
    PS4 = 'ps4'
    PC = 'pc'
    XBOX_ONE = 'xboxone'
    SWITCH = 'switch'


def get_start_url(platform_name):
    """Return starting url for parse metacritic site by platform_name"""
    return 'https://www.metacritic.com/browse/games/score/metascore/all/%s/filtered' % platform_name


def get_games_urls(soup):
    """Return list of games urls by BeautifulSoup html page"""
    game_div_list = soup.find('ol', class_='list_product_condensed').find_all('div', class_='product_title')
    links = []
    for div in game_div_list:
        a = div.find('a').get('href')
        link = METACRITIC_HOSTNAME + a
        links.append(link)
    return links


def get_next_page_url(soup):
    """Return url of next page of games list by BeautifulSoup html page
        or None if page doesn't exist"""
    a = soup.find('a', class_='action', rel='next')
    return METACRITIC_HOSTNAME + a.get('href') if a else None


def get_all_games_urls(platform_name=PlatformName.PS4):
    """Return all urls of games by platform"""
    current_url = get_start_url(platform_name)
    links = []
    while current_url is not None:
        game_urls, current_url = iter_all_games_urls(current_url)
        links.extend(game_urls)

    return links


@unexcepted_result
def iter_all_games_urls(url):
    """ Unexcepted iteration for method get_all_games_urls.
    Return list of game urls and next_url link
    """
    print(url)
    html = get_html(url)
    soup = BeautifulSoup(html, 'lxml')
    game_urls = get_games_urls(soup)
    next_url = get_next_page_url(soup)

    return game_urls, next_url
