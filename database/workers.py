import logging
from sqlalchemy.orm import Session

from database import ENGINE, SESSION, session
from database.models import Game, User, UserReview, Critic, CriticReview


_logger = logging.Logger(__name__)
_fileHandler = logging.FileHandler(filename='db_log')
_fileHandler.setFormatter(logging.Formatter('%(asctime)s %(processName)-10s %(name)s %(levelname)-8s %(message)s'))
_logger.addHandler(_fileHandler)
_logger.setLevel(level=logging.DEBUG)


def session_none_kwargs(func):
    def wrapper(*args, **kwargs):
        if 'session' not in kwargs:
            kwargs['session'] = SESSION()

        func(*args, **kwargs)

    return wrapper


def create_or_update_game(game_info):
    game_db = session.query(Game).filter(Game.url == game_info.url).first()
    if game_db:
        game_db.metascore = game_info.metascore
        game_db.userscore = game_info.userscore
    else:
        game = Game(
            titlename=game_info.title, url=game_info.url,
            metascore=game_info.metascore, userscore=game_info.userscore
        )

        session.add(game)


def create_base_review(review, game_info, user_clazz, review_clazz):
    # Check game in db
    game_db = session.query(Game).filter(Game.url == game_info.url).first()
    if not game_db:
        _logger.error('Game %s doesnt exist in db', game_info.url)

    # Get user db instance
    user_db = get_user(review.name, review.url, user_clazz, session=session)
    if not user_db:
        create_user(review.name, review.url, user_clazz, session=session)
        user_db = get_user(review.name, review.url, user_clazz, session=session)

    # Check review in db
    review_db = session.query(review_clazz).filter(review_clazz.user_id == user_db.id).\
        filter(review_clazz.game_id == game_db.id).first()
    if review_db is not None and review_db.score != review.score:
        _logger.error(
            "Review exist and scores not equal: %s, %s, %s, %s, %s",
             user_db.id, game_db.id, review.name, review.url, game_info.url
        )

    # Create and save instance review in db if review doesnt exist
    if review_db is None:
        user_review = review_clazz(user_id=user_db.id, game_id=game_db.id, score=review.score)
        session.add(user_review)


def create_critic_review(review, game_info):
    _logger.info('Critic review creat %s, %s, %s, %s', review.name, review.url, review.score, game_info.url)
    create_base_review(review, game_info, user_clazz=Critic, review_clazz=CriticReview)


def create_user_review(review, game_info):
    _logger.info('User review creat %s, %s, %s, %s', review.name, review.url, review.score, game_info.url)
    create_base_review(review, game_info, user_clazz=User, review_clazz=UserReview)


def get_user(name, url, user_clazz, session):
    return (
        (url and session.query(user_clazz).filter(user_clazz.url == url).first())
        or session.query(user_clazz).filter(user_clazz.name == name).first()
    )


@session_none_kwargs
def create_user(name, url, user_clazz, session=None):
    user = user_clazz(name=name, url=url or None)
    session.add(user)
