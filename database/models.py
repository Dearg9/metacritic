from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.schema import ForeignKey
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = 'users'

    id = Column(Integer(), primary_key=True)
    name = Column(String(), unique=True)
    url = Column(String(), unique=True, nullable=True)


class Critic(Base):
    __tablename__ = 'critics'

    id = Column(Integer(), primary_key=True)
    name = Column(String(), unique=True)
    url = Column(String(), unique=True, nullable=True)


class Game(Base):
    __tablename__ = 'games'

    id = Column(Integer(), primary_key=True)
    titlename = Column(String())
    url = Column(String(), unique=True)
    metascore = Column(Float())
    userscore = Column(Float())


class CriticReview(Base):
    __tablename__ = 'critic_reviews'

    id = Column(Integer(), primary_key=True)
    user_id = Column(Integer(), ForeignKey('critics.id'))
    game_id = Column(Integer(), ForeignKey('games.id'))
    score = Column(Float())


class UserReview(Base):
    __tablename__ = 'user_reviews'

    id = Column(Integer(), primary_key=True)
    user_id = Column(Integer(), ForeignKey('users.id'))
    game_id = Column(Integer(), ForeignKey('games.id'))
    score = Column(Float())
