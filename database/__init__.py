from sqlalchemy.engine import create_engine
from sqlalchemy.orm import sessionmaker


ENGINE = create_engine('sqlite:///metactitcs.db')
SESSION = sessionmaker(bind=ENGINE)
session = SESSION()
