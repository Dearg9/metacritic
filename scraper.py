import logging
import typing
from dataclasses import dataclass

from game_parser import GameInfo, get_game_info
from users_parser import get_all_user_reviews
from critics_parser import get_all_critic_reviews
from review_parser import BaseReview
from metacritic import PlatformName, get_all_games_urls
from database import workers, session


@dataclass
class ScrapingGameInfo:
    """This class describe review of users or critics"""
    game_info: GameInfo
    user_reviews: typing.List[BaseReview]
    critic_reviews: typing.List[BaseReview]


def scrap_metacritic_games(platform_name=PlatformName.PS4, save_db=False, save_file=True):
    file_result = None
    if save_file:
        file_result = open('result.txt', 'w')

    for game_url in get_all_games_urls(platform_name):
        game_info = get_game_info(game_url)
        user_reviews = get_all_user_reviews(game_url)
        critic_reviews = get_all_critic_reviews(game_url)
        scrap_result = ScrapingGameInfo(game_info=game_info, user_reviews=user_reviews,
                                        critic_reviews=critic_reviews)
        if file_result:
            file_result.write(str(scrap_result))
            file_result.write(',')

        if save_db:
            workers.create_or_update_game(game_info)
            for sub_review in user_reviews:
                workers.create_user_review(sub_review, game_info)

            for sub_review in critic_reviews:
                workers.create_critic_review(sub_review, game_info)

            session.commit()

    if file_result:
        file_result.close()
